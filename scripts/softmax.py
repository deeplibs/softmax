import numpy as np

# output -> network output
output = np.array([0, 10, 15, 18, 20])
softmax = np.exp(output) / sum( np.exp(output) )

np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
print 'Softmax = {}'.format( softmax )
for i in range( len(softmax) ):
    print 'Class = {}, score = {}, confidence = {:.4f}'.format( i, output[i], softmax[i] )

print 'Softmax sum = {}'.format( np.sum(softmax) )



# Softmax loss
# Cross-entropy
# p = right distribution
# q = wrong distribution
# H(p,q) = - sum(p * log(q))


print '\nSoftmax loss: case 1 - right label'

right_class = 5
labels = np.zeros( len(output) )
labels[right_class - 1] = 1

print 'Log(softmax) = {}'.format( np.log(softmax) )
print 'Labels = {}'.format( labels )
cross_entropy = - sum( labels * np.log(softmax) )
print 'Cross-entropy = {}'.format( cross_entropy )




print '\nSoftmax loss: case 2 - wrong label'

right_class = 3
labels = np.zeros( len(output) )
labels[right_class - 1] = 1

print 'Log(softmax) = {}'.format( np.log(softmax) )
print 'Labels = {}'.format( labels )
cross_entropy = - sum( labels * np.log(softmax) )
print 'Cross-entropy = {}'.format( cross_entropy )
