### Dependencies
 - python-numpy


### License
Creative Commons    
Attribution-NonCommercial-ShareAlike    
CC BY-NC-SA 4.0    
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode    


### Acknowledgement
Copyright © Thomio Watanabe    
All rights reserved
